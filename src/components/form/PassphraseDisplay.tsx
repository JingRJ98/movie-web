import { useRef, useState } from "react";
import { useCopyToClipboard, useMountedState } from "react-use";

import { Icon, Icons } from "../Icon";

export function PassphraseDisplay(props: { mnemonic: string[] }) {
  const { mnemonic } = props;

  const [, copy] = useCopyToClipboard();

  const [hasCopied, setHasCopied] = useState(false);
  const isMounted = useMountedState();

  const timeout = useRef<ReturnType<typeof setTimeout>>();

  function copyMnemonic() {
    const s = mnemonic.join(" ");
    copy(s);
    setHasCopied(true);
    if (timeout.current) clearTimeout(timeout.current);
    timeout.current = setTimeout(
      // 当组件还存在时 才会重置状态, 如果组件已经不在了, 不再动状态
      () => isMounted() && setHasCopied(false),
      1000,
    );
  }

  return (
    <div className="rounded-lg border border-authentication-border/50 ">
      <div className="px-4 py-2 flex justify-center border-b border-authentication-border/50">
        <button
          type="button"
          className="text-authentication-copyText hover:text-authentication-copyTextHover transition-colors flex gap-2 items-center cursor-pointer"
          onClick={copyMnemonic}
        >
          <Icon
            icon={hasCopied ? Icons.CHECKMARK : Icons.COPY}
            className={hasCopied ? "text-xs" : ""}
          />
          <span className="text-sm">点击复制</span>
        </button>
      </div>
      <div className="px-4 py-4 grid grid-cols-3 text-sm sm:text-base sm:grid-cols-4 gap-2">
        {mnemonic.map((word) => (
          <div
            className="rounded-md py-2 bg-authentication-wordBackground text-white font-medium text-center"
            key={word}
          >
            {word}
          </div>
        ))}
      </div>
    </div>
  );
}

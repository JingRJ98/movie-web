import { useTranslation } from "react-i18next";

import { MetaResponse } from "@/backend/accounts/meta";
import { Button } from "@/components/buttons/Button";
import { LargeCard, LargeCardButtons } from "@/components/layout/LargeCard";
import { Loading } from "@/components/layout/Loading";

interface TrustBackendPartProps {
  onNext?: (meta: MetaResponse) => void;
}

export function TrustBackendPart(props: TrustBackendPartProps) {
  const result = {
    loading: false,
    value: {
      name: "测试名称",
      description: "描述",
    },
  };
  const { t } = useTranslation();

  let cardContent = (
    <>
      <h3 className="text-white font-bold text-lg">
        {t("auth.trust.failed.title")}
      </h3>
      <p>{t("auth.trust.failed.text")}</p>
    </>
  );
  if (result.value)
    cardContent = (
      <>
        <h3 className="text-white font-bold text-lg">{result.value.name}</h3>
        {result.value.description ? (
          <p className="text-center">{result.value.description}</p>
        ) : null}
      </>
    );
  if (result.loading) cardContent = <Loading />;

  return (
    <LargeCard>
      <div className="border border-authentication-border rounded-xl px-4 py-8 flex flex-col items-center space-y-2 my-8">
        {cardContent}
      </div>
      <LargeCardButtons>
        <Button
          theme="purple"
          onClick={() => result.value && props.onNext?.(result.value)}
        >
          {t("auth.trust.yes")}
        </Button>
      </LargeCardButtons>
    </LargeCard>
  );
}

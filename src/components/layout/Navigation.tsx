import classNames from "classnames";
import { Link } from "react-router-dom";

import { NoUserAvatar, UserAvatar } from "@/components/Avatar";
import { LinksDropdown } from "@/components/LinksDropdown";
import Lightbar from "@/components/Lightbar";
import { useAuth } from "@/hooks/auth/useAuth";
import { useBannerSize } from "@/stores/banner";

import { BrandPill } from "./BrandPill";

// 页面上方header的导航组件
export function Navigation() {
  const bannerHeight = useBannerSize();
  const { loggedIn } = useAuth();

  return (
    <>
      {/* lightbar */}
      <div
        className="absolute inset-x-0 top-0 flex h-[88px] items-center justify-center"
        style={{
          top: `${bannerHeight}px`,
        }}
      >
        <div className="absolute inset-x-0 -mt-[22%] flex items-center sm:mt-0">
          <Lightbar />
        </div>
      </div>

      {/* content */}
      <div
        className="fixed pointer-events-none left-0 right-0 z-[60] top-0 min-h-[150px]"
        style={{
          top: `${bannerHeight}px`,
        }}
      >
        <div className={classNames("fixed left-0 right-0 flex items-center")}>
          <div className="px-7 py-5 relative z-[60] flex flex-1 items-center justify-between">
            <div className="flex items-center space-x-1.5 ssm:space-x-3 pointer-events-auto">
              <Link
                className="block tabbable rounded-full text-xs ssm:text-base"
                to="/"
              >
                <BrandPill clickable />
              </Link>
            </div>
            <div className="relative pointer-events-auto">
              <LinksDropdown>
                {loggedIn ? <UserAvatar withName /> : <NoUserAvatar />}
              </LinksDropdown>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

import { Component, useState } from "react";
import { useTranslation } from "react-i18next";

import { ButtonPlain } from "@/components/buttons/Button";
import { Icons } from "@/components/Icon";
import { IconPill } from "@/components/layout/IconPill";
import { Title } from "@/components/text/Title";
import { Paragraph } from "@/components/utils/Text";
import { ErrorLayout } from "@/components/layout/ErrorLayout";
import { ErrorCardInPlainModal } from "@/pages/errors/ErrorCard";

function ErrorPart(props: { error: any; errorInfo: any }) {
  const { t } = useTranslation();
  const [showErrorCard, setShowErrorCard] = useState(false);

  const maxLineCount = 5;
  const errorLines = (props.errorInfo.componentStack || "")
    .split("\n")
    .slice(0, maxLineCount);

  const error = `${props.error.toString()}\n${errorLines.join("\n")}`;

  return (
    <div className="relative flex min-h-screen flex-1 flex-col">
      <div className="flex h-full flex-1 flex-col items-center justify-center p-5 text-center">
        <ErrorLayout maxWidth="max-w-2xl w-9/10">
          <IconPill icon={Icons.EYE_SLASH}>{t("errors.badge")}</IconPill>
          <Title>{t("errors.title")}</Title>

          <Paragraph>{props.error.toString()}</Paragraph>
          <ErrorCardInPlainModal
            show={showErrorCard}
            onClose={() => setShowErrorCard(false)}
            error={error}
          />

          <div className="flex gap-3">
            <ButtonPlain
              theme="secondary"
              className="mt-6 p-2.5 md:px-12"
              onClick={() => window.location.reload()}
            >
              {t("errors.reloadPage")}
            </ButtonPlain>
            <ButtonPlain
              theme="purple"
              className="mt-6 p-2.5 md:px-12"
              onClick={() => setShowErrorCard(true)}
            >
              {t("errors.showError")}
            </ButtonPlain>
          </div>
        </ErrorLayout>
      </div>
    </div>
  );
}

interface ErrorBoundaryState {
  error?: {
    error: any;
    errorInfo: any;
  };
}

export default class ErrorBoundary extends Component<
  Record<string, unknown>,
  ErrorBoundaryState
> {
  constructor(props: { children: any }) {
    super(props);
    this.state = {
      error: undefined,
    };
  }

  componentDidCatch(error: any, errorInfo: any) {
    console.error("Render error caught", error, errorInfo);
    this.setState((s) => ({
      ...s,
      error: {
        error,
        errorInfo,
      },
    }));
  }

  render() {
    if (!this.state.error) return this.props.children as any;

    return (
      <ErrorPart
        error={this.state.error.error}
        errorInfo={this.state.error.errorInfo}
      />
    );
  }
}

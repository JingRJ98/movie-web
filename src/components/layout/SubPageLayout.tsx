import { Navigation } from "@/components/layout/Navigation";

export function SubPageLayout(props: { children: React.ReactNode }) {
  return (
    <div
      className="bg-background-main"
      style={{
        backgroundImage:
          "linear-gradient(to bottom, var(--tw-gradient-from), var(--tw-gradient-to) 800px)",
      }}
    >
      {/* Main page */}
      <div className="flex min-h-screen flex-col">
        <div style={{ flex: "1 0 auto" }}>
          <Navigation />
          <div className="mt-40 relative">{props.children}</div>
        </div>
        <footer className="mt-16 border-t border-type-divider py-16 md:py-8">
          <div className="mx-auto max-w-full px-8 w-[1300px] sm:px-16 grid md:grid-cols-2 gap-16 md:gap-8">
            底部数据
          </div>
        </footer>
      </div>
    </div>
  );
}

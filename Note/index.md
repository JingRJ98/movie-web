# 懒加载组件
react-lazy-with-preload库
```jsx
import { ReactElement, Suspense, lazy, useEffect } from "react";
import { lazyWithPreload } from "react-lazy-with-preload";

const DeveloperPage = lazy(() => import("@/pages/DeveloperPage"));
const TestView = lazy(() => import("@/pages/developer/TestView"));
const PlayerView = lazyWithPreload(() => import("@/pages/PlayerView"));
const SettingsPage = lazyWithPreload(() => import("@/pages/Settings"));

PlayerView.preload();
SettingsPage.preload();

function App() {
  return (
    <Layout>
      <Routes>
        <Route
          path="/media/:media"
          element={
            <LegacyUrlView>
              <Suspense fallback={null}>
                <PlayerView />
              </Suspense>
            </LegacyUrlView>
          }
        />
        <Route
          path="/settings"
          element={
            <Suspense fallback={null}>
              <SettingsPage />
            </Suspense>
          }
        />
        <Route path="/dev" element={<DeveloperPage />} />
        <Route path="/dev/video" element={<VideoTesterView />} />
        {process.env.NODE_ENV === "development" ? (
          <Route path="/dev/test" element={<TestView />} />
        ) : null}
      </Routes>
    </Layout>
  );
}

export default App;
```

# Divider
使用hr封装
```jsx
import classNames from "classnames";

export function Divider(props: { marginClass?: string }) {
  return (
    <hr
      className={classNames(
        "w-full h-px border-0 bg-utils-divider bg-opacity-50",
        props.marginClass ?? "my-8",
      )}
    />
  );
}
```


# 环境变量
Vite 在一个特殊的 import.meta.env 对象上暴露环境变量。这里有一些在所有情况下都可以使用的内建变量：

import.meta.env.MODE: {string} 应用运行的模式。

import.meta.env.BASE_URL: {string} 部署应用时的基本 URL。他由base 配置项决定。

import.meta.env.PROD: {boolean} 应用是否运行在生产环境（使用 NODE_ENV='production' 运行开发服务器或构建应用时使用 NODE_ENV='production' ）。

import.meta.env.DEV: {boolean} 应用是否运行在开发环境 (永远与 import.meta.env.PROD相反)。

import.meta.env.SSR: {boolean} 应用是否运行在 server 上。

Vite 使用 dotenv 从你的 环境目录 中的下列文件加载额外的环境变量：
.env                # 所有情况下都会加载
.env.local          # 所有情况下都会加载，但会被 git 忽略
.env.[mode]         # 只在指定模式下加载
.env.[mode].local   # 只在指定模式下加载，但会被 git 忽略

为了防止意外地将一些环境变量泄漏到客户端，只有以 VITE_ 为前缀的变量才会暴露给经过 vite 处理的代码。例如下面这些环境变量：


VITE_SOME_KEY=123
DB_PASSWORD=foobar
只有 VITE_SOME_KEY 会被暴露为 import.meta.env.VITE_SOME_KEY 提供给客户端源码，而 DB_PASSWORD 则不会。

console.log(import.meta.env.VITE_SOME_KEY) // 123
console.log(import.meta.env.DB_PASSWORD) // undefined

# 按步骤区分的组件
```jsx
import { useState } from "react";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
import { useNavigate } from "react-router-dom";

import { MetaResponse } from "@/backend/accounts/meta";
import { SubPageLayout } from "@/pages/layouts/SubPageLayout";
import {
  AccountCreatePart,
  AccountProfile,
} from "@/pages/auth/AccountCreatePart";
import { PassphraseGeneratePart } from "@/pages/auth/PassphraseGeneratePart";
import { TrustBackendPart } from "@/pages/auth/TrustBackendPart";
import { VerifyPassphrase } from "@/pages/auth/VerifyPassphrasePart";
import { PageTitle } from "@/pages/util/PageTitle";

function CaptchaProvider(props: {
  siteKey: string | null;
  children: React.ReactNode;
}) {
  if (!props.siteKey) return props.children;
  return (
    <GoogleReCaptchaProvider reCaptchaKey={props.siteKey}>
      {props.children}
    </GoogleReCaptchaProvider>
  );
}

export function RegisterPage() {
  const navigate = useNavigate();
  const [step, setStep] = useState(0);
  const [mnemonic, setMnemonic] = useState<null | string>(null);
  const [account, setAccount] = useState<null | AccountProfile>(null);
  const [siteKey, setSiteKey] = useState<string | null>(null);

  return (
    <CaptchaProvider siteKey={siteKey}>
      <SubPageLayout>
        <PageTitle subpage k="global.pages.register" />
        {step === 0 ? (
          <TrustBackendPart
            onNext={(meta: MetaResponse) => {
              setSiteKey(
                meta.hasCaptcha && meta.captchaClientKey
                  ? meta.captchaClientKey
                  : null,
              );
              setStep(1);
            }}
          />
        ) : null}
        {step === 1 ? (
          <PassphraseGeneratePart
            onNext={(m) => {
              setMnemonic(m);
              setStep(2);
            }}
          />
        ) : null}
        {step === 2 ? (
          <AccountCreatePart
            onNext={(a) => {
              setAccount(a);
              setStep(3);
            }}
          />
        ) : null}
        {step === 3 ? (
          <VerifyPassphrase
            hasCaptcha={!!siteKey}
            mnemonic={mnemonic}
            userData={account}
            onNext={() => {
              navigate("/");
            }}
          />
        ) : null}
      </SubPageLayout>
    </CaptchaProvider>
  );
}
```

# 一键复制内容到剪贴板
伪代码
```jsx
const Icons = {
  checkmark: `<svg xmlns="http://www.w3.org/2000/svg" height="1em" width="1em" fill="currentColor" viewBox="0 0 24 24"><path d="M9 22l-10-10.598 2.798-2.859 7.149 7.473 13.144-14.016 2.909 2.806z" /></svg>`,
  copy: `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy"><rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect><path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path></svg>`,
}

import { useCopyToClipboard, useMountedState } from "react-use";

const [, copy] = useCopyToClipboard();
const isMounted = useMountedState();
const [hasCopied, setHasCopied] = useState(false);

const timeout = useRef<ReturnType<typeof setTimeout>>();

function copyMnemonic() {
  // 储存数据到剪贴板
  copy(props.mnemonic);
  setHasCopied(true);
  if (timeout.current) clearTimeout(timeout.current);
  timeout.current = setTimeout(() => isMounted() && setHasCopied(false), 500);
}
<button
  type="button"
  className="text-authentication-copyText hover:text-authentication-copyTextHover transition-colors flex gap-2 items-center cursor-pointer"
  onClick={() => copyMnemonic()}
>
  <Icon
    icon={hasCopied ? Icons.CHECKMARK : Icons.COPY}
    className={hasCopied ? "text-xs" : ""}
  />
  <span className="text-sm">复制</span>
</button>

/// 补充Icon组件
export const Icon = memo((props: IconProps) => {
  return (
    <span
      dangerouslySetInnerHTML={{ __html: 'svg的字符串' }} // 也可以使用其他的svg方案 核心是copy的实现
    />
  );
});
```
react-use库的useCopyToClipboardhooks支持将内容放到剪贴板中

# 复杂大数据的diff
通过自定义的hook来实现多个数据的整合和修改判断
一个数据的设置 恢复 是否发生改变
```js
function useDerived<T>(
  initial: T,
): [T, Dispatch<SetStateAction<T>>, () => void, boolean] {
  const [overwrite, setOverwrite] = useState<T | undefined>(undefined);
  // 这个useEffect的作用是当初始值变化的时候 就重新将overwrite设为undefined 这样相当于重置
  useEffect(() => {
    setOverwrite(undefined);
  }, [initial]);
  const changed = useMemo(
    () => !isEqual(overwrite, initial) && overwrite !== undefined,
    [overwrite, initial],
  );
  const setter = useCallback<Dispatch<SetStateAction<T>>>(
    (inp) => {
      // 如果不是函数就直接用这个变量修改
      if (!(inp instanceof Function)) setOverwrite(inp);
      // 如果是这个函数就用这个函数修改
      else setOverwrite((s) => inp(s !== undefined ? s : initial));
    },
    [initial, setOverwrite],
  );

  // 如果overwrite被设为undefined就是用原始值
  const data = overwrite === undefined ? initial : overwrite;

  const reset = useCallback(() => setOverwrite(undefined), [setOverwrite]);

  return [data, setter, reset, changed];
}
```
多个数据的diff方案也可以通过一个hook来进行
在这个新的hook中应用上面的useDerived 对每个数据都有一个数据, 设置函数, 恢复函数, 是否改变
将是否改变和恢复单个数据整合起来再暴露出去
```js
export function useDerived<T>(
  initial: T,
): [T, Dispatch<SetStateAction<T>>, () => void, boolean] {
  const [overwrite, setOverwrite] = useState<T | undefined>(undefined);
  // 这个useEffect的作用是当初始值变化的时候 就重新将overwrite设为undefined 这样相当于重置
  useEffect(() => {
    setOverwrite(undefined);
  }, [initial]);
  const changed = useMemo(
    () => !isEqual(overwrite, initial) && overwrite !== undefined,
    [overwrite, initial],
  );
  const setter = useCallback<Dispatch<SetStateAction<T>>>(
    (inp) => {
      // 如果不是函数就直接用这个变量修改
      if (!(inp instanceof Function)) setOverwrite(inp);
      // 如果是这个函数就用这个函数修改
      else setOverwrite((s) => inp(s !== undefined ? s : initial));
    },
    [initial, setOverwrite],
  );

  // 如果overwrite被设为undefined就是用原始值
  const data = overwrite === undefined ? initial : overwrite;

  const reset = useCallback(() => setOverwrite(undefined), [setOverwrite]);

  return [data, setter, reset, changed];
}

export function useSettingsState(
  theme: string | null,
  appLanguage: string,
  subtitleStyling: SubtitleStyling,
  deviceName: string,
  proxyUrls: string[] | null,
  backendUrl: string | null,
  profile:
    | {
        colorA: string;
        colorB: string;
        icon: string;
      }
    | undefined,
  enableThumbnails: boolean,
) {
  const [proxyUrlsState, setProxyUrls, resetProxyUrls, proxyUrlsChanged] =
    useDerived(proxyUrls);
  const [backendUrlState, setBackendUrl, resetBackendUrl, backendUrlChanged] =
    useDerived(backendUrl);
  const [themeState, setTheme, resetTheme, themeChanged] = useDerived(theme);
  const [
    appLanguageState,
    setAppLanguage,
    resetAppLanguage,
    appLanguageChanged,
  ] = useDerived(appLanguage);
  const [subStylingState, setSubStyling, resetSubStyling, subStylingChanged] =
    useDerived(subtitleStyling);
  const [
    deviceNameState,
    setDeviceNameState,
    resetDeviceName,
    deviceNameChanged,
  ] = useDerived(deviceName);
  const [profileState, setProfileState, resetProfile, profileChanged] =
    useDerived(profile);
  const [
    enableThumbnailsState,
    setEnableThumbnailsState,
    resetEnableThumbnails,
    enableThumbnailsChanged,
  ] = useDerived(enableThumbnails);

  // 一键重置所有改动
  function reset() {
    resetTheme();
    resetAppLanguage();
    resetSubStyling();
    resetProxyUrls();
    resetBackendUrl();
    resetDeviceName();
    resetProfile();
    resetEnableThumbnails();
  }

  // 有一个改动就意味着需要保存/重置全部数据
  const changed =
    themeChanged ||
    appLanguageChanged ||
    subStylingChanged ||
    deviceNameChanged ||
    backendUrlChanged ||
    proxyUrlsChanged ||
    profileChanged ||
    enableThumbnailsChanged;

  return {
    reset,
    changed,
    theme: {
      state: themeState,
      set: setTheme,
      changed: themeChanged,
    },
    appLanguage: {
      state: appLanguageState,
      set: setAppLanguage,
      changed: appLanguageChanged,
    },
    subtitleStyling: {
      state: subStylingState,
      set: setSubStyling,
      changed: subStylingChanged,
    },
    deviceName: {
      state: deviceNameState,
      set: setDeviceNameState,
      changed: deviceNameChanged,
    },
    proxyUrls: {
      state: proxyUrlsState,
      set: setProxyUrls,
      changed: proxyUrlsChanged,
    },
    backendUrl: {
      state: backendUrlState,
      set: setBackendUrl,
      changed: backendUrlChanged,
    },
    profile: {
      state: profileState,
      set: setProfileState,
      changed: profileChanged,
    },
    enableThumbnails: {
      state: enableThumbnailsState,
      set: setEnableThumbnailsState,
      changed: enableThumbnailsChanged,
    },
  };
}
```

# 国旗图标方案
['en-US', 'hi', 'fa-IR', 'he-IL', 'ja-JP', 'ko-KR', 'pa', 'zh-CN']
```jsx
import classNames from "classnames";

import { getCountryCodeForLocale } from "@/utils/language";
import "flag-icons/css/flag-icons.min.css";

export interface FlagIconProps {
  country?: string;
  langCode?: string;
}

export function FlagIcon(props: FlagIconProps) {
  let countryCode: string | null = props.country ?? null;
  if (props.langCode) countryCode = getCountryCodeForLocale(props.langCode);

  if (props.langCode === "tok")
    return (
      <div className="w-8 h-6 rounded bg-[#c8e1ed] flex justify-center items-center">
        <img src="/flags/tokiPona.svg" className="w-7 h-5" />
      </div>
    );

  if (props.langCode === "pirate")
    return (
      <div className="w-8 h-6 rounded bg-[#2E3439] flex justify-center items-center">
        <img src="/flags/skull.svg" className="w-4 h-4" />
      </div>
    );

  if (props.langCode === "minion")
    return (
      <div className="w-8 h-6 rounded bg-[#ffff1a] flex justify-center items-center">
        <div className="w-4 h-4 border-2 border-gray-500 rounded-full bg-white flex justify-center items-center">
          <div className="w-1.5 h-1.5 rounded-full bg-gray-900 relative">
            <div className="absolute top-0 left-0 w-1 h-1 bg-white rounded-full transform -translate-x-1/3 -translate-y-1/3" />
          </div>
        </div>
      </div>
    );

  // Galicia - Not a country (Is a region of Spain) so have to add the flag manually
  if (props.langCode === "gl-ES")
    return (
      <div className="w-8 h-6 rounded bg-[#2E3439] flex justify-center items-center">
        <img src="/flags/galicia.svg" className="rounded" />
      </div>
    );

  let backgroundClass = "bg-video-context-flagBg";
  if (countryCode === "np") backgroundClass = "bg-white";

  return (
    <span
      className={classNames(
        "!w-8 min-w-8 h-6 rounded overflow-hidden bg-cover bg-center block fi",
        backgroundClass,
        countryCode ? `fi-${countryCode}` : undefined,
      )}
    />
  );
}
```

# 界面需要一个缩略图和一个全局图的场景下的方案
可以写两个组件, 但是互斥显示, 用同一个flag来表示切换到缩放和全局
```jsx
const [fullscreenPreview, setFullscreenPreview] = useState(false);
<CaptionPreview
  show
  styling={props.styling}
  onToggle={() => setFullscreenPreview((s) => !s)}
/>
<CaptionPreview
  show={fullscreenPreview}
  fullscreen
  styling={props.styling}
  onToggle={() => setFullscreenPreview((s) => !s)}
/>
```


* 注: 语言设置和主题设置只是保存了对应的变量 还未真正涉及到相关原理









import { Helmet } from "react-helmet-async";
import { useTranslation } from "react-i18next";

import { Button } from "@/components/buttons/Button";
import { ErrorLayout } from "@/components/layout/ErrorLayout";

export function NotFoundPart() {
  const { t } = useTranslation();

  return (
    <div className="relative flex flex-1 flex-col">
      <Helmet>
        <title>{t("notFound.badge")}</title>
      </Helmet>
      <div className="flex h-full flex-1 flex-col items-center justify-center p-5 text-center">
        <ErrorLayout>
          <Button
            href="/"
            theme="purple"
            padding="md:px-12 p-2.5"
            className="mt-6"
          >
            {t("notFound.goHome")}
          </Button>
        </ErrorLayout>
      </div>
    </div>
  );
}

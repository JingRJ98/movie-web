import { ReactElement, Suspense, useEffect } from "react";
import { lazyWithPreload } from "react-lazy-with-preload";
import { Route, Routes, useLocation, useNavigate } from "react-router-dom";

import { convertLegacyUrl, isLegacyUrl } from "@/backend/metadata/getmeta";
import usePing from "@/hooks/usePing";
import VideoTesterView from "@/pages/developer/VideoTesterView";
import { NotFoundPart } from "@/pages/errors/NotFoundPart";
import { HomePage } from "@/pages/HomePage";
import { LoginPage } from "@/pages/Login";
import { RegisterPage } from "@/pages/Register";
import { Layout } from "@/setup/Layout";
import { useHistoryListener } from "@/stores/history";
import { LanguageProvider } from "@/stores/language";

const PlayerView = lazyWithPreload(() => import("@/pages/PlayerView"));
const SettingsPage = lazyWithPreload(() => import("@/pages/Settings"));

PlayerView.preload();
SettingsPage.preload();

function LegacyUrlView({ children }: { children: ReactElement }) {
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const url = location.pathname;
    if (!isLegacyUrl(url)) return;
    convertLegacyUrl(location.pathname).then((convertedUrl) => {
      navigate(convertedUrl ?? "/", { replace: true });
    });
  }, [location.pathname, navigate]);

  if (isLegacyUrl(location.pathname)) return null;
  return children;
}

function App() {
  useHistoryListener();
  usePing();

  return (
    <Layout>
      <LanguageProvider />
      <Routes>
        {/* pages */}
        <Route
          path="/media/:media"
          element={
            <LegacyUrlView>
              <Suspense fallback={null}>
                <PlayerView />
              </Suspense>
            </LegacyUrlView>
          }
        />
        <Route
          path="/media/:media/:season/:episode"
          element={
            <LegacyUrlView>
              <Suspense fallback={null}>
                <PlayerView />
              </Suspense>
            </LegacyUrlView>
          }
        />
        {/* done 1 */}
        <Route path="/browse/:query?" element={<HomePage />} />
        {/* done 1 */}
        <Route path="/" element={<HomePage />} />
        {/* done */}
        <Route path="/register" element={<RegisterPage />} />
        {/* done */}
        <Route path="/login" element={<LoginPage />} />
        <Route
          path="/settings"
          element={
            <Suspense fallback={null}>
              <SettingsPage />
            </Suspense>
          }
        />
        <Route path="/dev/video" element={<VideoTesterView />} />
        {/* <Route path="/admin" element={<Admin />} /> */}
        {/* done */}
        <Route path="*" element={<NotFoundPart />} />
      </Routes>
    </Layout>
  );
}

export default App;

import { generateMnemonic } from "@scure/bip39";
import { wordlist } from "@scure/bip39/wordlists/english";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { Button } from "@/components/buttons/Button";
import { PassphraseDisplay } from "@/components/form/PassphraseDisplay";
import {
  LargeCard,
  LargeCardButtons,
  LargeCardText,
} from "@/components/layout/LargeCard";

interface PassphraseGeneratePartProps {
  onNext?: (mnemonic: string[]) => void;
}

export function PassphraseGeneratePart(props: PassphraseGeneratePartProps) {
  const mnemonic = useMemo(() => {
    const arr = [];
    for (let i = 0; i < 12; i++) {
      const element = (Math.random() * 26) >> 0;
      const aCode = "a".charCodeAt(0);
      const c = String.fromCharCode(aCode + element);
      arr.push(c);
    }
    return arr;
  }, []);
  const { t } = useTranslation();

  return (
    <LargeCard>
      <LargeCardText title={t("auth.generate.title")}>
        {t("auth.generate.description")}
      </LargeCardText>
      <PassphraseDisplay mnemonic={mnemonic} />

      <LargeCardButtons>
        <Button theme="purple" onClick={() => props.onNext?.(mnemonic)}>
          {t("auth.generate.next")}
        </Button>
      </LargeCardButtons>
    </LargeCard>
  );
}

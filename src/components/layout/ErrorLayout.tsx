import classNames from "classnames";
import { ReactNode } from "react";

export function ErrorLayout(props: {
  children: ReactNode;
  maxWidth?: string;
  noInnerContainer?: boolean;
}) {
  const { children, maxWidth, noInnerContainer } = props;

  if (noInnerContainer) {
    return (
      <div className="w-full h-full flex justify-center items-center flex-col">
        {children}
      </div>
    );
  }

  return (
    <div className="w-full h-full flex justify-center items-center flex-col">
      <div
        className={classNames(
          "w-full p-6 text-center flex flex-col items-center",
          maxWidth ?? "max-w-[28rem]",
        )}
      >
        {children}
      </div>
    </div>
  );
}

import { useEffect, useState } from "react";
import { Helmet } from "react-helmet-async";
import { useTranslation } from "react-i18next";

import { Navigation } from "@/components/layout/Navigation";
import { useDebounce } from "@/hooks/useDebounce";
import { useSearchQuery } from "@/hooks/useSearchQuery";
import { BookmarksPart } from "@/pages/home/BookmarksPart";
import { SearchPart } from "@/pages/home/SearchPart";
import { WatchingPart } from "@/pages/home/WatchingPart";
import { SearchListPart } from "@/pages/search/SearchListPart";
import { SearchLoadingPart } from "@/pages/search/SearchLoadingPart";

function useSearch(search: string) {
  // 只要输入框有变化 自动进入searching 和loading状态
  // ui方面优先显示loading效果
  // 每次loading 500ms之后自动结束

  const [searching, setSearching] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const debouncedSearch = useDebounce<string>(search, 500);

  useEffect(() => {
    setSearching(search !== "");
    setLoading(search !== "");
  }, [search]);

  // 500ms后自动结束loading
  useEffect(() => {
    setLoading(false);
  }, [debouncedSearch]);

  return {
    loading,
    searching,
  };
}

export function HomePage() {
  const { t } = useTranslation();
  const searchParams = useSearchQuery();
  const [search] = searchParams;
  const s = useSearch(search);

  return (
    <div className="flex min-h-screen flex-col">
      <div style={{ flex: "1 0 auto" }}>
        <Navigation />
        <div className="mb-16 sm:mb-24">
          <Helmet>
            <title>{t("global.name")}</title>
          </Helmet>
          <SearchPart searchParams={searchParams} />
        </div>

        <div className="mx-auto max-w-full px-8 w-[900px] sm:px-8">
          {s.loading ? (
            <SearchLoadingPart />
          ) : s.searching ? (
            <SearchListPart searchQuery={search} />
          ) : (
            <>
              <BookmarksPart />
              <WatchingPart />
            </>
          )}
        </div>
      </div>
      <footer className="mt-16 border-t border-type-divider py-16 md:py-8">
        <div className="mx-auto max-w-full px-8 w-[1300px] sm:px-16 grid md:grid-cols-2 gap-16 md:gap-8">
          底部数据
        </div>
      </footer>
    </div>
  );
}

import en from "@/assets/locales/en.json";
import ja from "@/assets/locales/ja.json";
import zh from "@/assets/locales/zh.json";

export const locales = {
  en,
  ja,
  zh,
};
export type Locales = keyof typeof locales;

import { useEffect, useRef } from "react";
import { useBannerStore } from "@/stores/banner";

export default function usePing() {
  const updateOnline = useBannerStore((s) => s.updateOnline);
  const ref = useRef<boolean>(true);

  useEffect(() => {
    let counter = 0;

    let abort: null | AbortController = null;
    // 每5秒执行一次检查联网的请求
    // 但是在线时，每 5 * 10 = 50 秒才会进行一次请求
    const interval = setInterval(() => {
      counter++;
      if (ref.current) {
        if (counter < 10) return;
      }
      counter = 0;

      if (abort) abort.abort();
      abort = new AbortController();
      const signal = abort.signal;
      fetch("/ping.txt", { signal })
        .then(() => {
          updateOnline(true);
          ref.current = true;
        })
        .catch((err) => {
          if (err.name === "AbortError") return;
          updateOnline(false);
          ref.current = false;
        });
    }, 5000);

    return () => {
      clearInterval(interval);
      if (abort) abort.abort();
    };
  }, [updateOnline]);
}
